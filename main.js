import './style.css'

import * as THREE from 'three';

import { AmbientLight } from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';





// scene == container
const scene = new THREE.Scene();

const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

const renderer = new THREE.WebGLRenderer({
  canvas: document.querySelector('#bg'),
});

renderer.setPixelRatio( window.devicePixelRatio );
renderer.setSize( window.innerWidth, window.innerHeight );
camera.position.setZ(30);

renderer.render(scene, camera);

// GOLD RING
const geometry = new THREE.TorusGeometry( 12, 2, 16, 100 );
const material = new THREE.MeshStandardMaterial({ color: 0xffd700, flatShading: false, metalness: 0.7, roughness: 0.5, reflectivity: 5 });
const torus = new THREE.Mesh( geometry, material );

scene.add(torus);


// ILUMINAÇÕES
const pointLight = new THREE.PointLight(0xffffff)
pointLight.position.set(10,10,5)

const ambientLight = new THREE.AmbientLight(0xffffff)


scene.add(pointLight, ambientLight);

  // HELPER ILUMINAÇÕES
const lightHelper = new THREE.PointLightHelper(pointLight)
const gridHelper = new THREE.GridHelper(200, 50);
scene.add(lightHelper, gridHelper);

// CONTROL CAMERA
const controls = new OrbitControls(camera, renderer.domElement);

// ARRAY CLONE RANDOM
function addStar() {
  const geometry = new THREE.SphereGeometry(0.25, 24, 24);
  const material = new THREE.MeshStandardMaterial({ color: 0xffffff });
  const star = new THREE.Mesh( geometry, material );

  const [x, y, z] = Array(3).fill().map(() => THREE.MathUtils.randFloatSpread( 100 ));

  star.position.set(x, y, z);
  scene.add(star)
}

Array(400).fill().forEach(addStar);

// BACKGROUND
const spaceTexture = new THREE.TextureLoader().load('https://res.cloudinary.com/alkimera/image/upload/v1623075795/space3_nuxogi.jpg');
scene.background = spaceTexture;



// Avatar
const marcioTexture = new THREE.TextureLoader().load('https://res.cloudinary.com/alkimera/image/upload/v1623081358/favicon_g3bjxw.ico');

const marcio = new THREE.Mesh(
  new THREE.BoxGeometry(7, 7, 7),
  new THREE.MeshBasicMaterial({ map: marcioTexture })
);

scene.add(marcio)

// MOON
const moonTexture =  new THREE.TextureLoader().load('https://res.cloudinary.com/alkimera/image/upload/v1623075730/moon_re06oz.jpg');
const normalTexture=  new THREE.TextureLoader().load('https://res.cloudinary.com/alkimera/image/upload/v1623075746/normal_b2hwxy.jpg');

const moon = new THREE.Mesh(
  new THREE.SphereGeometry(15, 64, 64),
  new THREE.MeshStandardMaterial({
    map: moonTexture,
    normalMap: normalTexture
  })
);

scene.add(moon)

moon.position.z = 40;
moon.position.y = 10;
moon.position.setX(-20);


function moveCamera() {

  const t = document.body.getBoundingClientRect().top;
  moon.rotation.x += 0.05
  moon.rotation.y += 0.075
  moon.rotation.z += 0.05

  marcio.rotation.y += 0.05
  marcio.rotation.z += 0.05
  
  camera.position.z = t * -0.01;
  camera.position.x = t * -0.01;
  camera.position.y = t * -0.01;


}

document.body.onscroll = moveCamera


function animate() {
  requestAnimationFrame(animate);

  torus.rotation.x += 0.005;
  torus.rotation.y += 0.0005;
  torus.rotation.z += 0.005;

  controls.update();

  renderer.render(scene, camera);
}

animate();



// document.querySelector('#app').innerHTML = `
//   <h1>Hello Vite!</h1>
//   <a href="https://vitejs.dev/guide/features.html" target="_blank">Documentation</a>
// `
